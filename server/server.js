const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());

app.post("/login", (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  if (!username || !password) {
    res.send({ error: "please enter username & password" });
  }
  res.send({
    name: username,
    db: {
      host: "localhost",
      port: "5984",
      user: "root",
      pass: "root",
      name: "pouch_poc",
    },
  });
});

app.listen(port, () => {
  console.log(`server listening on port ${port}`);
});
