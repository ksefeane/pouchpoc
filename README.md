# Pouch POC

## setup

### using docker compose

0. (couldn't get it to work, port issues)

1. from the root path. run: docker compose up

### manual mode

1. open 3 separate terminals

2. from the root path, run: sh db/run_docker_db.sh (to run the pouch db docker)

3. go to root/app, run: npm run dev (to run the app)

4. go to root/server, run: node server.js (to run the login server)

## enable cors

1. after running the docker db, go to: http://localhost:5984/\_utils/

2. login with username: root, password: root

3. navigate to http://localhost:5984/\_utils/#\_config/nonode@nohost (or click on the settings icon, although they all look the same. click on this icon -> ⚙️)

4. click on CORS, then click on enable cors (this allow http request to the db)

## login

1. login using any username & password. the server will return db configs (this is just for demo purposes)

## create sample account & transactions

1. after login, enter any username and click the button. this will create a sampe account & transaction for the username.

2. click on load accounts to display accounts

3. click on account to display account transactions

## if things go wrong

1. right click on the chrome refresh page and 'empty cache and hard reload'

2. check CORS settings in the db UI

3. it be like that sometimes

4. call the 'destroyPouch()' method in the code, then log in again
