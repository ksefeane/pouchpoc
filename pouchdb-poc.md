# Synchronized Vue Accounts view

## Introduction

Application user interfaces often need to fetch sensitive information which can often only be done once a user is authenticated. In an environment with many microservices (like ours), this can lead to a number of issues:

  * multiple backend requests (10s or 100s) to build up the initial views
  * the user might have to wait for all these requests to complete in order to have a useful UI to interact with
  * a failure in any of these requests must be handled properly or the UI ends up being fragile with seemingly random "technical error" messages
  * on slow and congested networks the above issues can compound possibly making the application unusable

To get around these issues, the amount of data that is initially fetched is typically reduced which can lead to other shorter pauses throughout the user journey where similar concerns arise again, if only on a smaller scale.

There are a number of approaches to tackle this issue and the subject of this project is an experiment to explore one based on using [PouchDB](https://pouchdb.com/). In this approach, once a user is authenticated the client application would then synchronize with a copy of a database containing all the data the front-end would require. Once this sync is complete, the front-end should require almost no further queries from the backend apart from streaming use cases. The aim of this project is to assess the potential of this approach to:

 * require only a single request to get user data needed throughout the user journey
 * make loading data all/nothing, reducing the number of edge cases to handle
 * be an approach that is portable across platforms
 * make transitions between views near instant
 * give the user interface complete control of filtering, sorting and formatting of data
 * decouple the front ends from backend API structure contraints
 * _(bonus)_  make it trivial for the user interface to be usable offline

The output of this should be a minimal proof of concept for assessing the feasibility of this approach.

## Overview

The application in question must be a [VueJS](https://vuejs.org/) based web interface that authenticates a user and then presents some basic information. The authentication response will contain all the details needed for connecting to the backend [CouchDB](https://couchdb.apache.org/) instance. The application should then initiate a sync from the backedn CouchDB instance and use the data it contains to build the views.

The backend server only has one route (`/login`) that takes a `POST` request. The request body must look like the below:

```json
{
  "username": "<string>",
  "password": "<string>"
}
```

The response will have one of the following HTTP status codes:

  * `200` - authentication is successful
  * `400` - there is an issue with the request
  * `401` - authentication failed

A successful authentication request will generate a response that looks like the following:

```json
{
  "name": "<string>",

  "db": {
    "user": "<string>",
    "pass": "<string>",
    "name": "<string>",
    "port": "<integer>",
    "host": "<string>"
  }
}
```

The DB credentials in the above response are what would be used to connect to the remote database that would be the source of the initial data. The user interface should then allow a user to view a list of accounts on their profile and then transactions by account as well.

Accounts can be found from the database as documents with the type `"account"`. Similarly, transactions have the type `"txn"`.


Account are documents with the following properties

| Field   | Type      |
| ------- | --------- |
| name    | string    |
| balance | float     |
| created | timestamp |

Transactions are documents with the following properties

| Field       | Type                                      |
| ----------- | ----------------------------------------- |
| timestamp   | timestamp                                 |
| account     | string - reference to an account document |
| description | string                                    |
| amount      | float                                     |

For faster development or iteration, a local instance of CouchDB can be run using the following:

```sh
docker run --rm -it -p '127.0.0.1:5984:5984' -e COUCHDB_USER=admin -e COUCHDB_PASSWORD=$MY_STRONG_PASSWORD apache/couchdb
```

A somewhat user friendly interface ([Fauxton](https://couchdb.apache.org/fauxton-visual-guide/#documentation)) is then available on the CouchDB server on the path `$SERVER_URL/_utils` (http://127.0.0.1:5984/_utils for the above server).