import type { DB_config, Account, Transaction } from "@/types/interfaces";
import type { fetchMethod } from "@/types/types";
import { fetchPromise } from "../request";

export class DB {
    private static _instance: DB;
    private static _host: string;
    private static _port: string;
    private static _user: string;
    private static _pass: string;
    private static _dbName: string;

    private static get user() {
        if (!this._instance) throw "db auth error! please initialize DB class"
        return this._user;
    }

    private static get pass() {
        if (!this._instance) throw "db auth error! please initialize DB class"
        return this._pass;
    }

    private static get url() {
        if (!this._instance) throw "url error! please initialize DB class"
        return `http://${this._host}:${this._port}`
    }

    private static get dbname() {
        if (!this._instance) throw "dbname error! please initialize DB class"
        return this._dbName
    }

    private static get authHeaders() {
        return new Headers({
            'Authorization': `Basic ${btoa(`${this.user}:${this.pass}`)}`,
            'Content-Type': 'application/json'
        })
    }

    static init({host, port, user, pass, name}: DB_config) {
        if (!this._instance) {
            this._host = host;
            this._port = port;
            this._user = user;
            this._pass = pass;
            this._dbName = name;
            this._instance = new DB();
            console.log(`init DB 🟢`);
        }
        return this._instance;
    }

    static connect(configs: DB_config) {
        this.init(configs)
        return this;
    }

    private static requestJson(method: fetchMethod, body?: any, path?: string,) {
        const requestOptions: RequestInit = {
            method: method,
            headers: this.authHeaders
        }
        if (body) {
            requestOptions.body = JSON.stringify(body);
        }
        const url = `${this.url}/${this.dbname}${path ?? ''}`
        return fetchPromise(url, requestOptions);
    }

    static async createDatabase() {
        console.log(`create ${this.dbname} DB`)
        return await this.requestJson("PUT");
    }

    static async find(obj: any) {
        const response: any = await this.requestJson("POST", {
            "selector": obj,
            "skip": 0,
            "execution_stats": true
        }, '/_find')
        return response.docs
    }

    static async put(obj: any) {
        return await this.requestJson("POST", obj)
    }
}