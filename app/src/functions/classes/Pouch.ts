import type { DB_config } from "@/types/interfaces";
import { DB } from "./DB";
import { dbName } from "@/configs/db";

export class Pouch {
    private static _instance: Pouch;
    private static _pouch: any;
    private static _config: DB_config;

    private static get db() {
        return this._pouch;
    }

    private static set config(newConfig: DB_config) {
        this._config = newConfig
        this.db.put({
            _id: 'config',
            ...newConfig
        })
        .then(() => {
            console.log('pouch db config created')
        })
        .catch((e: any) => {
            if (e.status === 409) {
                console.log('pouch db config found');
            } else {
                console.error(e)
            }
        })
    }

    static get config() {
        if (!this._instance) throw `${dbName} not configured`
        return this._config;
    }

    static init(config: DB_config) {
        if (!this._instance) {
            this._instance = new Pouch
            this._pouch = new PouchDB(dbName);
            this.config = config
            DB.init(config)
            console.log(`init ${dbName} 🟢`);
        }
        return this._instance;
    }

    private static async getConfig() {
        return await this.db.get('config')
    }

    static async connect() {        
        if (!this._instance) {
            this._instance = new Pouch
            this._pouch = new PouchDB(dbName);
            try {
                const config = await this.getConfig();
                this.config = config
                console.log(`connect ${dbName} 🟢`); 
            } catch (e) {
                console.error(`connect ${dbName} 🔴`, e)
            }
        }
        return this._instance;
    }

    static async destroy() {
        if (!this._instance) {
            await this.connect();
        }
        this.db.destroy()
        console.log(`${dbName} destroyed`);
    }


    static async put(obj: any) {
        this.connect();
        const id  = await this.db.put(obj)
        DB.connect(this.config).put(obj);
        return id;
    }

    static async get(id: string) {
        this.connect();
        return this.db.get(id);
    }

    // this is a special functions
    // the find by object function was not available in pouch db 
    // so I called the method from couchDB directly
    static async find(obj: any) {
        this.connect()
        return DB.connect(this.config).find(obj)
    }

    static async setup(docArray: any[]) {
        docArray.map(async (doc: any) => {
            await this.put(doc);
            console.log('create doc', doc);
        })
    }
}