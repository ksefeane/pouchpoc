export const fetchPromise = async (url: string, requestOptions?: RequestInit) =>
    new Promise((resolve, reject) => {
        fetch(url, requestOptions)
            .then(response => response.json()
                .then(data => resolve(data))
                .catch(err => reject(err))
            )
            .catch(e => reject(e))
    })