import type { AccountType, TransactionType } from "./types";

export interface DB_config {
    host: string;
    port: string;
    user: string;
    pass: string;
    name: string;
}

export interface Account {
    _id: string;
    name: string;
    balance: number;
    created: string;
    type: AccountType;
}

export interface Transaction {
    _id: string;
    account: string;
    amount: number;
    description: string;
    timestamp: string;
    type: TransactionType;
}