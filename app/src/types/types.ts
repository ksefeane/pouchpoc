export type fetchMethod = "GET" | "POST" | "PUT";

export type AccountType = "account";

export type TransactionType = "txn";