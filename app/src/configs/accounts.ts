import type { Account, Transaction } from "@/types/interfaces";

export const account: Account = {
    _id: "donald",
    name: "donald",
    balance: 1000.00,
    created: new Date().toLocaleDateString() + " - " +  new Date().toLocaleTimeString(),
    type: "account"
}

export const transactions: Transaction[] = [
    {
        _id: "donald",
        account: "donald",
        amount: 400,
        description: "pay money to checkers",
        timestamp: new Date().toLocaleDateString() + " - " + new Date().toLocaleTimeString(),
        type: "txn",
    },
    {
        _id: "donald",
        account: "donald",
        amount: 1000,
        description: "pay for mr price",
        timestamp: new Date().toLocaleDateString() + " - " +  new Date().toLocaleTimeString(),
        type: "txn",
    },
    {
        _id: "donald",
        account: "donald",
        amount: 700,
        description: "pay for petrol",
        timestamp: new Date().toLocaleDateString() + " - " +  new Date().toLocaleTimeString(),
        type: "txn",
    }
]